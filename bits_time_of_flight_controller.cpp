#include "bits_time_of_flight_controller.h"

bits_TimeOfFlightController::bits_TimeOfFlightController(const uint16_t max_distance): max_distance(max_distance) {}


void bits_TimeOfFlightController::init() {
    bool found_any_sensors = false;

    // Deaktivieren aller Sensoren
    for (int i = 0; i < SENSOR_COUNT; i++)
    {
        pinMode(sensors[i].shutdown_pin, OUTPUT);
        
        digitalWrite(sensors[i].shutdown_pin, LOW);
    }

    delay(10);

    // Die Sensoren nach einander aktivieren und initalisieren
    for (int i = 0; i < SENSOR_COUNT; i++)
    {
        digitalWrite(sensors[i].shutdown_pin, HIGH);

        delay(100);

        if (sensors[i].psensor->begin(sensors[i].id, false, sensors[i].pwire, sensors[i].sensor_config))
        {
            found_any_sensors = true;
        }
        else
        {
            _print_tof_(i);
            _print_tof_(F(": failed to start\n"));
        }
    }
    if (!found_any_sensors)
    {
        _println_tof_("No valid sensors found");

        while (1)
        {
            ;
        }
    }
}

int bits_TimeOfFlightController::get_distance_mm(int index) {
    VL53L0X_RangingMeasurementData_t measure;

    sensors[index].psensor->rangingTest(&measure, false); // 'true' um debug Informationen auszugeben

    int distance;

    if ((measure.RangeStatus != 4) && (measure.RangeMilliMeter < max_distance))
    {
      distance = measure.RangeMilliMeter;
      _print_tof_(index);
      _print_tof_(" Sensor Entfernung (cm): ");
      _println_tof_(distance);
    }
    else
    {
      _println_tof_(" Ausserhalb des Messbereichs ");
      distance = max_distance+1;
    }

    return distance;
}

int bits_TimeOfFlightController::get_distance_cm(int index) {
    int distance = get_distance_mm(index)/10;

    return distance;
}