/*      BITS-i ToF-Sensor shematic
 *    
 *     +----#2------------#3----+
 *     |                        |
 *     |        <------         |
 *     | #0      BITS-i         |
 *     |                        |
 *     |                        |
 *     +----#1------------#4----+
 *
 * #0 - #4: Time of Flight Sensoren  
 */


// #define DEBUG_TOF
#include "bits_time_of_flight_controller.h"

bits_TimeOfFlightController tof_controller(4000);
int values[5] = {};

void setup()
{
    Serial.begin(115200);
    Wire.begin();

    tof_controller.init();
}

void loop()
{
    values[0] = tof_controller.get_distance_cm(0);
    values[1] = tof_controller.get_distance_cm(1);
    values[2] = tof_controller.get_distance_cm(2);
    values[3] = tof_controller.get_distance_cm(3);
    values[4] = tof_controller.get_distance_cm(4);

    Serial.print(values[0]);
    Serial.print(" cm");
    Serial.print("\t");
    Serial.print(values[1]);
    Serial.print(" cm");
    Serial.print("\t");
    Serial.print(values[2]);
    Serial.print(" cm");
    Serial.print("\t");
    Serial.print(values[3]);
    Serial.print(" cm");
    Serial.print("\t");
    Serial.print(values[4]);
    Serial.println(" cm");
}