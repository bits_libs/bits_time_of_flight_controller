/*
 * Klasse: bits_TimeOfFlightController
 *
 * Zur erzeugung eines Arrays gefüllt mit Objekten für die Ansteuerung von mehreren VL53L0X Time of Flight Sensoren
 *
 * O.-T. Altan, Januar 2022
 *
 */

#ifndef TIME_OF_FLIGHT_CONTROLLER_H
#define TIME_OF_FLIGHT_CONTROLLER_H

#include "Adafruit_VL53L0X.h"

#ifdef DEBUG_TOF
#define _println_tof_(x) Serial.println(x)
#define _print_tof_(x) Serial.print(x)
#else
#define _print_tof_(x)
#define _println_tof_(x)
#endif

class bits_TimeOfFlightController
{
public:
  bits_TimeOfFlightController(const uint16_t max_distance);
  void init();
  int get_distance_cm(int index);
  int get_distance_mm(int index);

private:
  const uint16_t max_distance = 4000;                              // Maximal messbare Distanz
  static const int SENSOR_COUNT = 5;                               // Sensoren Anzahl
  int TOF_XSHUTS [SENSOR_COUNT] = {0, 8, 9, 23, 29};               // XShut Pins der einzelnen Time of Flight Sensoren
  int TOF_ADDRESSES [SENSOR_COUNT] = {0x30, 0x31, 0x32, 0x33, 0x34}; // Adresse der einzelnen Time of Flight Sensoren

  // Struktur der Sensor Liste
  typedef struct
  {
    Adafruit_VL53L0X *psensor; // Zeiger zum Objekt
    TwoWire *pwire;
    int id;           // Adresse des Sensors
    int shutdown_pin; // XShut Pin des Sensors
    Adafruit_VL53L0X::VL53L0X_Sense_config_t
        sensor_config; // Option wie der Sensor verwendet werden soll
    uint16_t range;    // Wertebereich für den continuous mode
    uint8_t sensor_status;
  } sensorList_t;

  // Sensor Objekte basierend auf der Adafruit Bibliothek
  Adafruit_VL53L0X tof_sensor_1;
  Adafruit_VL53L0X tof_sensor_2;
  Adafruit_VL53L0X tof_sensor_3;
  Adafruit_VL53L0X tof_sensor_4;
  Adafruit_VL53L0X tof_sensor_5;

  // Liste von Sensor Objekten
  sensorList_t sensors [SENSOR_COUNT] =
  {
    {&tof_sensor_1, &Wire, TOF_ADDRESSES[0], TOF_XSHUTS[0], Adafruit_VL53L0X::VL53L0X_SENSE_DEFAULT, 0, 0},
    {&tof_sensor_2, &Wire, TOF_ADDRESSES[1], TOF_XSHUTS[1], Adafruit_VL53L0X::VL53L0X_SENSE_DEFAULT, 0, 0},
    {&tof_sensor_3, &Wire, TOF_ADDRESSES[2], TOF_XSHUTS[2], Adafruit_VL53L0X::VL53L0X_SENSE_DEFAULT, 0, 0},
    {&tof_sensor_4, &Wire, TOF_ADDRESSES[3], TOF_XSHUTS[3], Adafruit_VL53L0X::VL53L0X_SENSE_DEFAULT, 0, 0},
    {&tof_sensor_5, &Wire, TOF_ADDRESSES[4], TOF_XSHUTS[4], Adafruit_VL53L0X::VL53L0X_SENSE_DEFAULT, 0, 0}
  };

};

#endif