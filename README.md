# **BITs Time of flight array**

Diese Bibliothek verwendet die Adafruit_VL53L0X Bibliothek um einen Array mit mehreren Time of Flight Objekten zu erstellen. Da die VL53L0X Time of Flight Sensoren alle die selbe Adresse besitzen, müssen diese nacheinander aktiviert und initialisiert werden. Dies soll mit dieser Bibliothek automatisch geschehen. 

Getestet mit:

- Arduino Mega Pro + 5x VL53L0X<br />
<br />

## **Vorausetzung**

- [Adafruit_VL53L0X](https://github.com/adafruit/Adafruit_VL53L0X) von Adafruit.<br />
<br />

## **Installation:**

Um diese Bibliothek verwenden zu können, muss dieses Repository geklont und in das Library-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung**:

Zur Verwendung siehe zunächst das Beispiel `measurement.ino`<br />
<br />

**Einbinden der Bibliothek:**

```arduino
#include "bits_time_of_flight_controller.h"
```

**Instanziieren:**

```arduino
bits_TimeOfFlightController tof_controller(max_distance);
```

- `max_distance`: Maximale Distanz in mm. Gemessene Abstände größer als diesen Wert werden als -1 zurück gegeben.<br />
<br />

**Zum messen mit den Time of Flight Sensoren wird folgende Methode verwendet:**

```arduino
int value = tof_controller.get_distance_mm(index);
int value = tof_controller.get_distance_cm(index);
```
- `index`: Time of Flight Sensor Nummer 0-4<br />
<br />

## **Anmerkungen**:

Um die Anzahl der Time of Flight Sensoren, die zugehörigen XShut Pin Belegungen, sowie die Adressen der Sensoren anzupassen, müssen in der `time_of_flight_controller.h` Datei folgende Parameter bearbeitet werden:

- `SENSOR_COUNT`: Anzahl der zu Verwendenden Time of Flight Sensoren.
- `TOF_XSHUTS`: Pinbelegungen der XShut Pins von den einzelnen Sensoren.
- `TOF_ADDRESSES`: Adressen der einzelnen Sensoren.

Möchte man mehr als 5 Time of Flight Sensoren verwenden, so müssen ebenfalls anpassungen an der Liste `sensors[]` gemacht, so wie weitere *Adafruit_VL53L0X* Objekte instanziiert werden.<br />
<br />

## **Quellen:**

- Adafruit_VL53L0X: vl53l0x.ino
- Adafruit_VL53L0X: vl53l0x_multi_extended.ino
